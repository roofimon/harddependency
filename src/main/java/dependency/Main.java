package dependency;

/**
 * Created by oo8 on 6/25/2017 AD.
 */
public class Main {
    public static void main(String[] args){
        EmailService emailService = new GmailService();
        RoofController controller = new RoofController();
        controller.setEmailService(emailService);
        controller.sendEmailToRoof();
    }
}
