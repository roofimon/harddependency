package dependency;

/**
 * Created by oo8 on 6/25/2017 AD.
 */
public interface EmailService {
    public void send(String message);

}
