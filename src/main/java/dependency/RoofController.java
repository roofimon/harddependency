package dependency;

/**
 * Created by oo8 on 6/25/2017 AD.
 */
public class RoofController {
    private EmailService emailService;
    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }
    public void sendEmailToRoof() {
        //this.emailService.send("Some Message"); uncomment this line if you want to see test fail !!!!
        this.emailService.send("Some Message");
    }
}
