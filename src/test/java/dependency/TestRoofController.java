package dependency;

import org.junit.Test;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;

/**
 * Created by oo8 on 6/25/2017 AD.
 */
public class TestRoofController {
    private final int ONCE = 1;
    @Test
    public void sendAnEmailToRoof() {
        //Arrange
        RoofController roofController = new RoofController();
        EmailService mockEmailService = new MockEmailService();
        roofController.setEmailService(mockEmailService);

        //Act
        roofController.sendEmailToRoof();

        //Assert
        verifyThat_send_MethodHasBeenCalledOnce(mockEmailService);


    }

    public void verifyThat_send_MethodHasBeenCalledOnce(EmailService mock){
        if( ((MockEmailService)mock).getCounter() != ONCE )
            fail("Expected that send should has been called once but "+ ((MockEmailService)mock).getCounter());
    }

    public class MockEmailService implements EmailService {
        public int counter = 0;
        public void send(String message) {
            this.counter++;
        }

        public int getCounter() {
            return this.counter;
        }
    }
}

